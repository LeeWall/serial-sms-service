package com.koinrocket.common;

public enum ResultEnum {

    UNKNOWN(999, "未知错误"), SUCCESS(1,"操作成功"), FAILED(0, "操作失败");

    public int code;
    public String message;

    ResultEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
