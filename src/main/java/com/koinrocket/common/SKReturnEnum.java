package com.koinrocket.common;

public enum SKReturnEnum {
    SUCCESS(0, "success or reply the max and available port"),
    INVALID_1(1, "invalid username or password"),
    INVALID_2(2, "invalid port"),
    NOT_RESISTERED(5, "sim not resistered"),
    TIMEOUNT(6, "timeout"),
    SEND_FAILED(7, "send sms failed"),
    NO_TEXT_PARAMETER(8, "no sms parameter"),
    NO_RECIPIENTS_PARAMETER(9, "no recipients"),
    PENDING_SMS(10, "have pending sms");

    private Integer code;

    private String desc;

    SKReturnEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
