package com.koinrocket.common;

public class Constant {

    public static final String DEVICE_IP_LIST = "device_ip_list";

    public static final String DEVICE_NUM = "device_num";

    public static final String DEVICE_PORT_NUM = "device_port_num";

    public static final String KEY = "112233445566778899";

    public static final String SEND_URL_TEMPLATE = "send_url_template";

    public static final String PORT_NOT_REGISTERED_LIMIT = "port_not_registered_limit";

    public static final String NOT_DEAL_SMS_NUM = "not_deal_message_num";

    public static final String RETRY_SMS_NUM = "retry_sms_num";

    public static final String RETRY_SMS_TIMES = "retry_sms_times";

    public static final String SMS_TEXT_LIMIT = "sms_text_length_limit";

    public static final String TEST_PHONE = "test_phone";

    public static final String COUNTRY_CODE = "country_code";

    public static final String COUNTRY_PHONE_ILLEGAL_MESSAGE = "phone_illegal_message";

    public static final String CALLBACK_SMS_URL = "callback_sms_url";

    public static final String CALLBACK_SMS_DEFAULT_URL = "https://koinrocket.id/mapi/sms/callback";

    public static final String DEVICE_IP = "192.168.55.151";

}
