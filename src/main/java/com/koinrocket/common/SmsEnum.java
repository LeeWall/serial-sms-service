package com.koinrocket.common;

public enum SmsEnum {

    WAIT(0,"短消息待处理"),SUCCESS(1,"发送成功"),FAILED(99,"发送失败");

    public int code;
    public String status;

    SmsEnum(int code, String status) {
        this.code = code;
        this.status = status;
    }
}
