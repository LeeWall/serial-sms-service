package com.koinrocket.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ModemListener implements ApplicationListener<ContextClosedEvent>{

    public static final Logger LOGGER = LoggerFactory.getLogger(ModemListener.class);


    @Autowired
    @Qualifier(value = "gatewayMap")
    private Map<String, SerialModemGateway> modemGatewayMap ;

    @Autowired
    @Qualifier(value = "serviceMap")
    private Map<String, Service> serviceMap;

    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {
       try {
           for (Map.Entry<String, SerialModemGateway> entry: modemGatewayMap.entrySet()){
               String serialPort = entry.getKey();
               SerialModemGateway gateway = entry.getValue();
               if (serviceMap.containsKey(serialPort)){
                   Service service = serviceMap.get(serialPort);
                   if (service != null){
                       service.stopService();
                       service.removeGateway(gateway);
                   }
               }
           }
       }catch (Exception e){
           LOGGER.error("停止短信服务出现异常", e);
       }
    }
}
