package com.koinrocket.service;

import com.koinrocket.dto.SmsQueueDTO;
import com.koinrocket.entity.SmsHistoryEntity;
import com.koinrocket.entity.SmsQueueEntity;

import java.util.List;

public interface ModemService {

    /**
     *
     * @param smsQueueEntity
     */
    void execute(SmsQueueEntity smsQueueEntity);

    /**
     *
     * @param smsQueueEntity
     */
    void retry(SmsQueueEntity smsQueueEntity);

    /**
     *
     * @param smsQueueDTO
     */
    String sendSms(SmsQueueDTO smsQueueDTO);


    /**
     *
     * @param phone
     * @return
     */
    List<SmsHistoryEntity>  receiveSms(String phone);
}
