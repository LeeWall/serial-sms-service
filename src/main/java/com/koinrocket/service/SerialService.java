package com.koinrocket.service;



import gnu.io.CommPortIdentifier;

import java.util.List;

public interface SerialService {


    /**
     *  检查所有串口
     * @return
     */
    List<CommPortIdentifier> findALlCommPort();

    /**
     * 检查串口是否可用
     * @param serialPortName
     * @return
     */
    boolean isSerialPortEnabled(String serialPortName);

    /**
     * 获得串口可用的波特率
     * @param serialPortName
     * @return -1表示失败
     */
    int getBaudRate(String serialPortName);

}
