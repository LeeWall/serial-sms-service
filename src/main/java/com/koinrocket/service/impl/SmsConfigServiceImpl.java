package com.koinrocket.service.impl;

import com.koinrocket.entity.SmsConfigEntity;
import com.koinrocket.repository.SmsConfigRepository;
import com.koinrocket.service.SmsConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

@Service
public class SmsConfigServiceImpl implements SmsConfigService {

    @Autowired
    private SmsConfigRepository smsConfigRepository;

    @Autowired
    @Qualifier(value = "configMap")
    private Map<String, String> configMap;

    @Override
    public void findAll() {
        List<SmsConfigEntity> smsConfigEntityList = smsConfigRepository.findAllConfigs();
        if (CollectionUtils.isEmpty(smsConfigEntityList)){
            return ;
        }
        for (SmsConfigEntity smsConfigEntity:smsConfigEntityList){
            configMap.put(smsConfigEntity.getKey(),smsConfigEntity.getValue());
        }
    }
}
