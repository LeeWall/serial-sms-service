package com.koinrocket.service.impl;

import com.koinrocket.bean.GatewayBean;
import com.koinrocket.bean.SKResponseBean;
import com.koinrocket.common.Constant;
import com.koinrocket.dto.SmsQueueDTO;
import com.koinrocket.entity.SmsHistoryEntity;
import com.koinrocket.entity.SmsQueueEntity;
import com.koinrocket.repository.MessageQueueRepository;
import com.koinrocket.repository.SmsHistoryRepository;
import com.koinrocket.service.ModemService;
import com.koinrocket.util.CallbackUtil;
import com.koinrocket.util.GsonUtil;
import com.koinrocket.util.SKUtils;
import com.koinrocket.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.*;
import org.smslib.crypto.AESKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * 短信服务
 */
@Service
@DependsOn(value = "devicePortDetection")
public class ModemServiceImpl  implements ModemService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ModemServiceImpl.class);

    @Autowired
    private ServiceUtil serviceUtil;

    @Autowired
    private MessageQueueRepository messageQueueRepository;

    @Autowired
    private SmsHistoryRepository smsHistoryRepository;

    @Autowired
    private SKUtils skUtils;

    @Autowired
    private CallbackUtil callbackUtil;

    @Autowired
    private GsonUtil gsonUtil;

    @Autowired
    @Qualifier(value = "configMap")
    private Map<String, String> configMap;

    @Autowired
    @Qualifier(value = "portCrossFailedMap")
    private Map<String, AtomicInteger> portNumMap;

    @Autowired
    @Qualifier(value = "portNotRegisterMap")
    private Map<String, AtomicInteger> portNotRegisterNumMap;

    @Autowired
    @Qualifier(value = "enableUsePortMap")
    private Map<String, AtomicInteger> enablePortMap;

    @Autowired
    @Qualifier(value = "notUsePortSet")
    private Set<String> notUseSet ;



    @Override
    public void execute(SmsQueueEntity smsQueueEntity) {
        String url = null;
        String portNum = null;
        boolean status = true;
        String result = null;
        String port = null;
        SKResponseBean skResponseBean = null;
        AtomicInteger tagStatus = null;
        int returnPut = 1;
        try {
            try {
                if (notUseSet.size() >= 31){
                    LOGGER.warn("the device does not continue to send sms");
                    return;
                }
                //寻找可用端口
                while (status){
                    int index = ThreadLocalRandom.current().nextInt(32)+1;
                    portNum = String.valueOf(index);
                    tagStatus = enablePortMap.get(portNum);
                    if ( tagStatus != null && tagStatus.intValue() == 1){
                        continue;
                    }
                    //端口18无法使用
                    if (index == 18){
                        continue;
                    }
                    enablePortMap.put(portNum, new AtomicInteger(tagStatus.incrementAndGet()));
                    status = false;
                    port = URLEncoder.encode(portNum, "utf-8");
                    String to = URLEncoder.encode(smsQueueEntity.getToPhone(), "utf-8");
                    String text = URLEncoder.encode(smsQueueEntity.getText().concat("."), "utf-8");
                    url = String.format(configMap.get(Constant.SEND_URL_TEMPLATE), Constant.DEVICE_IP,  port, to, text);
                    LOGGER.info("try messageId:{} url:{}",smsQueueEntity.getMessageId(), url);
                    TimeUnit.MILLISECONDS.sleep(200);
                }
            }catch (Exception e){
                LOGGER.error("port choose exception,{}",e);
            }
            try {
                long startTime = System.currentTimeMillis();
                //调用设备API
                smsQueueEntity.setTryCount(smsQueueEntity.getTryCount()+1).setSendTime(startTime).setUrl(url).setDevicePort(String.valueOf(portNum));
                //调用设备API，得到响应结果
                /*try {
                    result =  restTemplate.getForObject(url, String.class);
                }catch (Exception e){
                    LOGGER.error("call remote api exception",e);
                    smsQueueEntity.setApiExecuteTime(System.currentTimeMillis()-startTime);
                }*/
                result = skUtils.sendSms(url);
                smsQueueEntity.setApiExecuteTime(System.currentTimeMillis()-startTime);
                //返回状态解析
                skResponseBean = gsonUtil.GsonToBean(result, SKResponseBean.class);
                if(skResponseBean != null){
                    if (skResponseBean.getCode() != 0){
                        LOGGER.info("device api request failed , messageId:[{}],result:[{}]", smsQueueEntity.getMessageId(), skResponseBean.getReason());
                        smsQueueEntity.setStatus(99).setResultReason(skResponseBean.getReason()).setRetryStatus(0);
                    }else {
                        LOGGER.info("device api request succeed, messageId:[{}]", smsQueueEntity.getMessageId());
                        smsQueueEntity.setStatus(1).setResultReason(skResponseBean.getReason());
                    }
                }else {
                    LOGGER.info("not return result,messageId:[{}],result:[{}]", smsQueueEntity.getMessageId(),"connection refused");
                    smsQueueEntity.setStatus(99).setResultReason("connection refused").setRetryStatus(0);
                }
            }catch (Exception e){
                LOGGER.error("messageId:{},call device api exception,{}",smsQueueEntity.getMessageId(),e);
                smsQueueEntity.setStatus(99).setResultReason(e.getMessage()).setRetryStatus(0);
            }
            //调用回调通知后台短信发送成功
            try {
                smsQueueEntity.setCallbackCount(smsQueueEntity.getCallbackCount()+1).setCallbackTime(System.currentTimeMillis());
                Integer callbackResult = callbackUtil.callbackSMS(smsQueueEntity);
                if (callbackResult == 1){
                    smsQueueEntity.setCallbackStatus(1);
                }else {
                    smsQueueEntity.setCallbackStatus(99);
                }
            }catch (Exception e){
                smsQueueEntity.setCallbackStatus(99);
                LOGGER.error("call callback api exception,messageId:{},{}",e,smsQueueEntity.getMessageId());
            }
            //短信发送失败，判断原因是否为端口未注册
            try {
                if ("Send sms failed".equalsIgnoreCase(smsQueueEntity.getResultReason())){
                    if (portNumMap.containsKey(portNum)){
                        AtomicInteger num = portNumMap.get(portNum);
                        portNumMap.put(portNum, new AtomicInteger(num.incrementAndGet()));
                    }else {
                        portNumMap.put(portNum, new AtomicInteger(1));
                    }
                    int crossFailedTimes = portNumMap.get(portNum).intValue();
                    int limitTimes = configMap.get(Constant.PORT_NOT_REGISTERED_LIMIT) == null ? 80 :Integer.valueOf(configMap.get(Constant.PORT_NOT_REGISTERED_LIMIT));
                    if (crossFailedTimes >= limitTimes){
                        returnPut = 0;
                        notUseSet.add(portNum);
                    }
                }
            }catch (Exception e){
                LOGGER.error("check port exception,{}",e);
            }
            //如果是端口没注册
            if ("SIM Not Registered".equalsIgnoreCase(smsQueueEntity.getResultReason())||"Not Registered".equalsIgnoreCase(smsQueueEntity.getResultReason())){
                if (portNotRegisterNumMap.containsKey(portNum)){
                    portNotRegisterNumMap.get(portNum).incrementAndGet();
                }else {
                    portNotRegisterNumMap.put(portNum, new AtomicInteger(1));
                }
                int notRegisteredTimes = portNotRegisterNumMap.get(portNum).intValue();
                //int limitTimes = configMap.get(Constant.PORT_NOT_REGISTERED_LIMIT) == null ? 80 :Integer.valueOf(configMap.get(Constant.PORT_NOT_REGISTERED_LIMIT));
                if (notRegisteredTimes >= 100){
                    notUseSet.add(portNum);
                    returnPut = 0;
                }
            }
            //还原取出状态
            smsQueueEntity.setPutStatus(0);
            //更新短信状态
            messageQueueRepository.save(smsQueueEntity);
        } catch (Exception e) {
            LOGGER.error("messageId:{},sms send happened exception,{}",e);
            //发送失败还原取出状态和执行状态为失败
            smsQueueEntity.setPutStatus(0).setStatus(99).setRetryStatus(0).setTryCount(1).setSendTime(System.currentTimeMillis()).setResultReason(e.getMessage());
            messageQueueRepository.save(smsQueueEntity);
        } finally {
            if (returnPut == 1 && portNum != null){
                enablePortMap.put(portNum,new AtomicInteger(0));
            }
        }
    }

    @Override
    public String sendSms(SmsQueueDTO smsQueueDTO) {
        String messageId = UUID.randomUUID().toString().replace("-","");
        SmsQueueEntity smsQueueEntity = new SmsQueueEntity();
        smsQueueEntity.setFromPhone(smsQueueDTO.getFromPhone()).setToPhone(smsQueueDTO.getToPhone()).setText(smsQueueDTO.getText())
                .setStatus(0).setTryCount(0).setReceiveTime(System.currentTimeMillis()).setMessageId(messageId).setCallbackCount(0).
                setDevice(smsQueueDTO.getDevice()).setRetryCount(0).setPutStatus(0).setPriority(smsQueueDTO.getPriority());
        messageQueueRepository.save(smsQueueEntity);
        return messageId;
    }

    @Override
    public void retry(SmsQueueEntity smsQueueEntity) {
        String url = null;
        String portNum = null;
        boolean status = true;
        String port = null;
        String result = null;
        SKResponseBean skResponseBean = null;
        int returnPut = 1;
        long startTime =  System.currentTimeMillis();
        AtomicInteger tagStatus = null;
        try {
            if (notUseSet.size() >= 31){
                LOGGER.warn("the device does not continue to send sms");
                return;
            }
            try {
                while (status){
                    int index = ThreadLocalRandom.current().nextInt(32)+1;
                    portNum = String.valueOf(index);
                    tagStatus = enablePortMap.get(portNum);
                    if (tagStatus != null && tagStatus.intValue() == 1){
                        continue;
                    }
                    //端口18无法使用
                    if (index == 18){
                        continue;
                    }
                    enablePortMap.put(portNum, new AtomicInteger(tagStatus.incrementAndGet()));
                    status = false;
                    port = URLEncoder.encode(portNum, "utf-8");
                    String to = URLEncoder.encode(smsQueueEntity.getToPhone(), "utf-8");
                    String text = URLEncoder.encode(smsQueueEntity.getText().concat("."), "utf-8");
                    url = String.format(configMap.get(Constant.SEND_URL_TEMPLATE), Constant.DEVICE_IP,  port, to, text);
                    TimeUnit.MILLISECONDS.sleep(200);
                }
            }catch (Exception e){
                LOGGER.error("port choose exception,{}",e);
            }
            smsQueueEntity.setRetryCount(smsQueueEntity.getRetryCount()+1).setRetryTime(startTime).setDevicePort(portNum).setUrl(url);
            //调用设备API，得到响应结果
            /*try {
                result =  restTemplate.getForObject(url, String.class);
            }catch (Exception e){
                LOGGER.error("call remote api exception",e);
                smsQueueEntity.setApiExecuteTime(System.currentTimeMillis()-startTime);
            }*/
            try {
                result = skUtils.sendSms(url);
                skResponseBean = gsonUtil.GsonToBean(result, SKResponseBean.class);
                if (skResponseBean != null){
                    smsQueueEntity.setResultReason(skResponseBean.getReason());
                    if (skResponseBean.getCode() == 0){
                        LOGGER.info("retry to succeed,messageId:[{}]",smsQueueEntity.getMessageId());
                        smsQueueEntity.setStatus(1).setRetryStatus(1).setResultReason(skResponseBean.getReason() == null?"OK":skResponseBean.getReason());
                    }else {
                        LOGGER.info("retry to fail,messageId:[{}]",smsQueueEntity.getMessageId());
                        smsQueueEntity.setRetryStatus(99).setResultReason(skResponseBean.getReason());
                    }
                }else {
                    LOGGER.info("retry to not connect device, messageId:[{}],result:[{}]", smsQueueEntity.getMessageId(),"connection refused");
                    smsQueueEntity.setResultReason("connection refused").setRetryStatus(99);
                }
            }catch (Exception e){
                LOGGER.error("messageId:{},call device api exception,{}",smsQueueEntity.getMessageId(),e);
                smsQueueEntity.setResultReason("connection refused").setRetryStatus(99);
            }
            try {
                smsQueueEntity.setCallbackCount(smsQueueEntity.getCallbackCount()+1).setCallbackTime(System.currentTimeMillis());
                Integer retryCallbackResult = callbackUtil.callbackSMS(smsQueueEntity);
                if (retryCallbackResult == 1){
                    smsQueueEntity.setCallbackStatus(1);
                }else {
                    smsQueueEntity.setCallbackStatus(99);
                }
            }catch (Exception e){
                smsQueueEntity.setCallbackStatus(99);
                LOGGER.error("messageId:{},retry call callback api exception,{}",smsQueueEntity.getMessageId(),e);
            }
            //短信发送失败，判断原因是否为端口未注册
            try {
                if ("Send sms failed".equalsIgnoreCase(smsQueueEntity.getResultReason())){
                    if (portNumMap.containsKey(portNum)){
                        AtomicInteger num = portNumMap.get(portNum);
                        portNumMap.put(portNum, new AtomicInteger(num.incrementAndGet()));
                    }else {
                        portNumMap.put(portNum, new AtomicInteger(1));
                    }
                    int crossFailedTimes = portNumMap.get(portNum).intValue();
                    int limitTimes = configMap.get(Constant.PORT_NOT_REGISTERED_LIMIT) == null ? 80 :Integer.valueOf(configMap.get(Constant.PORT_NOT_REGISTERED_LIMIT));
                    if (crossFailedTimes >= limitTimes){
                        notUseSet.add(portNum);
                        returnPut = 0;
                    }
                }
            }catch (Exception e){
                LOGGER.error("check port exception,{}",e);
            }
            //如果是端口没注册
            if ("SIM Not Registered".equalsIgnoreCase(smsQueueEntity.getResultReason())||"Not Registered".equalsIgnoreCase(smsQueueEntity.getResultReason())){
                if (portNotRegisterNumMap.containsKey(portNum)){
                    portNotRegisterNumMap.get(portNum).incrementAndGet();
                }else {
                    portNotRegisterNumMap.put(portNum, new AtomicInteger(1));
                }
                int notRegisteredTimes = portNotRegisterNumMap.get(portNum).intValue();
                //int limitTimes = configMap.get(Constant.PORT_NOT_REGISTERED_LIMIT) == null ? 80 :Integer.valueOf(configMap.get(Constant.PORT_NOT_REGISTERED_LIMIT));
                if (notRegisteredTimes >= 100){
                    notUseSet.add(portNum);
                    returnPut = 0;
                }
            }
            //还原取出状态
            smsQueueEntity.setPutStatus(0);
            //更新短信状态
            messageQueueRepository.save(smsQueueEntity);
        } catch (Exception e) {
            LOGGER.error("messageId:{},retry to send sms exception,{}",smsQueueEntity.getMessageId(),e);
            smsQueueEntity.setPutStatus(0).setRetryStatus(99).setResultReason(e.getMessage()).
                    setRetryCount(smsQueueEntity.getRetryCount()+1).setApiExecuteTime(System.currentTimeMillis()-startTime);
            messageQueueRepository.save(smsQueueEntity);
        } finally {
            if (returnPut == 1 && portNum != null){
                enablePortMap.put(portNum,new AtomicInteger(0));
            }
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<SmsHistoryEntity> receiveSms(String phone) {
        List<SmsHistoryEntity> smsHistoryEntityList = new ArrayList<>();
        try {
            GatewayBean gatewayBean  = serviceUtil.adjustService(phone);
            if (gatewayBean.getStatus() == 0){
                LOGGER.error("没有可用的网关服务，无法接受短信");
                return null;
            }
            org.smslib.Service service = gatewayBean.getService();
            //接受新短信时回调
            service.setInboundMessageNotification(new InboundNotification());
            //gateway状态发生改变时回调
            service.setGatewayStatusNotification(new GatewayStatusNotification());
            //检测到不完整短信进行回调
            service.setOrphanedMessageNotification(new OrphanedMessageNotification());
            service.getKeyManager().registerKey(phone, new AESKey(new SecretKeySpec(Constant.KEY.getBytes(), "AES")));
            InboundMessage[] inboundMessages = service.readMessages(InboundMessage.MessageClasses.ALL);
            if (inboundMessages == null || inboundMessages.length == 0){
                return null;
            }
            for (InboundMessage inboundMessage: inboundMessages){
                String fromPhoneNumber = inboundMessage.getSmscNumber();
                //String fromPhoneNumber = inboundMessage.getOriginator();
                String text = inboundMessage.getText();
                long dateTime = inboundMessage.getDate().getTime();
                SmsHistoryEntity smsHistoryEntity = new SmsHistoryEntity();
                smsHistoryEntity.setFromPhoneNumber(fromPhoneNumber).setReceivePhoneNumber(phone).setText(text).setReceiveTime(dateTime);
                smsHistoryEntityList.add(smsHistoryEntity);
            }
            smsHistoryRepository.saveAll(smsHistoryEntityList);
        }  catch (Exception e) {
            LOGGER.error("收取短信异常",e);
        }
        return smsHistoryEntityList;
    }

    class InboundNotification implements IInboundMessageNotification {
        @Override
        public void process(AGateway gateway, Message.MessageTypes msgType, InboundMessage msg) {

        }
    }

    class GatewayStatusNotification implements IGatewayStatusNotification {
        @Override
        public void process(AGateway gateway, AGateway.GatewayStatuses oldStatus, AGateway.GatewayStatuses newStatus) {

        }
    }

    class OrphanedMessageNotification implements IOrphanedMessageNotification {
        @Override
        public boolean process(AGateway gateway, InboundMessage msg) {
            return false;
        }
    }
}
