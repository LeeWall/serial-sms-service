package com.koinrocket.service.impl;

import com.koinrocket.service.SerialService;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.SerialPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.server.PortInUseException;
import org.springframework.stereotype.Service;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@Service
public class SerialServiceImpl implements SerialService {

    public static final Logger LOGGER = LoggerFactory.getLogger(SerialServiceImpl.class);

    private int bauds[] = {115200, 57600, 38400, 19200, 9600};

    @Override
    public List<CommPortIdentifier> findALlCommPort() {
        List<CommPortIdentifier> commPortIdentifierList = new ArrayList<>();
        Enumeration<CommPortIdentifier> portList = CommPortIdentifier.getPortIdentifiers();
        while (portList.hasMoreElements()) {
            CommPortIdentifier portId = portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                commPortIdentifierList.add(portId);
            }
        }
        return commPortIdentifierList;
    }

    @Override
    public boolean isSerialPortEnabled(String serialPortName) {
        CommPortIdentifier portId = null;
        try {
            portId = CommPortIdentifier.getPortIdentifier(serialPortName);
        } catch (NoSuchPortException e) {
            LOGGER.error("找不到串口:" + serialPortName);
            return false;
        }
        SerialPort port = null;
        try {
            port = (SerialPort) portId.open(serialPortName, 2000);
            port.close();
        } catch (gnu.io.PortInUseException e) {
            LOGGER.info("串口 " + serialPortName + " 被占用");
            return false;
        }
        LOGGER.info("SerialPort: " + port.getName() + " : BaudRate=" + port.getBaudRate());
        return true;
    }

    @Override
    public int getBaudRate(String serialPortName) {
        try {
            CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(serialPortName);
            for (int i = 0; i < bauds.length; i++) {
                if (validBaud(portId, bauds[i], serialPortName)) {
                    return bauds[i];
                }
            }
        }catch (Exception e){
            LOGGER.error("获取波特率出现异常", e);
        }
        return -1;
    }

    /**
     * 验证串口波特率是否正确
     * @param portId
     * @param baud
     * @return
     */
    private boolean validBaud(CommPortIdentifier portId, int baud, String serialPortName) {
        SerialPort serialPort = null;
        InputStream inStream = null;
        OutputStream outStream = null;
        String response = null;
        int c = 0;
        try {
            serialPort = (SerialPort) portId.open(serialPortName, 2000);
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN);
            serialPort.setSerialPortParams(baud, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            inStream = serialPort.getInputStream();
            outStream = serialPort.getOutputStream();
            serialPort.enableReceiveTimeout(1000);
            c = inStream.read();
            while (c != -1) {
                c = inStream.read();
            }
            outStream.write('A');
            outStream.write('T');
            outStream.write('\r');
            c = inStream.read();
            while (c != -1) {
                response += (char) c;
                c = inStream.read();
            }
            if (response.indexOf("OK") >= 0) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("波特率检查异常",e);
        } finally {
            if (serialPort != null) {
                serialPort.close();
            }
        }
        return false;
    }
}
