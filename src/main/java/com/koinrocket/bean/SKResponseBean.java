package com.koinrocket.bean;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Setter
@Getter
@Accessors(chain = true)
public class SKResponseBean implements Serializable {

    private Integer code;

    private String reason;

    @SerializedName(value = "maxPort", alternate = {"max-port"})
    private Integer maxPort;

    private String port;

    @Override
    public String toString() {
        return "SKResponseBean{" +
                "code=" + code +
                ", reason='" + reason + '\'' +
                ", maxPort=" + maxPort +
                ", port='" + port + '\'' +
                '}';
    }
}
