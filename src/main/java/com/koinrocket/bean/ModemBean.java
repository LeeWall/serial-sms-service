package com.koinrocket.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@Accessors(chain = true)
public class ModemBean implements Serializable {

    /**
     * 是否开启短信猫服务
     */
    private boolean enabled ;

    /**
     * 串口名称
     */
    private String serialPort ;

    /**
     * 波特率
     */
    private int baudRate ;

    /**
     * 开发商
     */
    private String manufacturer ;

    /**
     * 型号
     */
    private String model ;

}
