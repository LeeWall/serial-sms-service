package com.koinrocket.bean;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class CallbackBean implements Serializable {

    private Integer code;

    private String msg;

    private String data;
}
