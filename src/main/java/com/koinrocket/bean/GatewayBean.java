package com.koinrocket.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.smslib.Service;

import java.io.Serializable;

@Setter
@Getter
@Accessors(chain = true)
public class GatewayBean implements Serializable {

    private Service service;

    private String serialPort;

    private String phoneNumber;

    private Integer status;
}
