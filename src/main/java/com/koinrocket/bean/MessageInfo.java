package com.koinrocket.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Setter
@Getter
@Accessors(chain = true)
public class MessageInfo implements Serializable {

    private Integer code;

    private String msg;

    private String to;

    private String messageId;

    private Long time;
}
