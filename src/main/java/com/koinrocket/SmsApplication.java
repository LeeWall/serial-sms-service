package com.koinrocket;

import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.servlet.MultipartConfigElement;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@EnableScheduling
@ServletComponentScan
public class SmsApplication {

    @Bean("gatewayMap")
    public Map<String, SerialModemGateway> initGateway(){
        Map<String, SerialModemGateway> modemGatewayMap = new ConcurrentHashMap<>();
        return modemGatewayMap;
    }

    @Bean("serviceMap")
    public Map<String, Service> initService(){
        Map<String, Service> serviceMap = new LinkedHashMap<>();
        return serviceMap;
    }

    @Bean("serialVector")
    public Vector<String> initEnAbleSerialPort(){
        Vector<String> vector = new Vector<>();
        return vector;
    }

    @Bean("configMap")
    public Map<String, String> initConfig(){
        Map<String, String> serviceMap = new ConcurrentHashMap<>();
        return serviceMap;
    }

    @Bean("occupiedDevicePortSet")
    public Set<Integer> devicePort() {
        Set<Integer> occupiedPortSet = new CopyOnWriteArraySet<>();
        return occupiedPortSet;
    }

    @Bean(value = "portCrossFailedMap")
    public Map<String, AtomicInteger> initCrossFailedDevicePort(){
        Map<String, AtomicInteger> portMap = new ConcurrentHashMap<>();
        return portMap;
    }

    @Bean(value = "portNotRegisterMap")
    public Map<String, AtomicInteger> initNotRegisterDevicePort(){
        Map<String, AtomicInteger> portMap = new ConcurrentHashMap<>();
        return portMap;
    }

    @Bean(value = "enableUsePortMap")
    public Map<String, AtomicInteger> enableUsePort(){
        Map<String, AtomicInteger> enablePortMap = Collections.synchronizedMap(new HashMap());
        return enablePortMap;
    }

    @Bean(value = "notUsePortSet")
    public Set<String> notUsePort(){
        Set<String> notUseSet = Collections.synchronizedSet(new HashSet<>());
        return notUseSet;
    }

    //java.io.IOException: The temporary upload location xxx is not valid
    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setLocation("D:\\sms-service\\temp");
        return factory.createMultipartConfig();
    }


    public static void main(String[] args) {
        SpringApplication.run(SmsApplication.class, args);
    }
}
