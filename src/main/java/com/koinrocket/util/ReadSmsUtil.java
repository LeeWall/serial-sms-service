package com.koinrocket.util;

import com.koinrocket.bean.GatewayBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.*;
import org.smslib.crypto.AESKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReadSmsUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(ReadSmsUtil.class);

    @Autowired
    private ServiceUtil serviceUtil;

    private static final String Key = "";

    public List<InboundMessage> receiveSMS(String phone) {
        List<InboundMessage> inboundMessageList = new ArrayList<>();
        InboundNotification inboundNotification = new InboundNotification();
        CallNotification callNotification = new CallNotification();
        GatewayStatusNotification statusNotification = new GatewayStatusNotification();
        OrphanedMessageNotification orphanedMessageNotification = new OrphanedMessageNotification();
        try {
            GatewayBean gatewayBean  = serviceUtil.defaultService();
            if (gatewayBean == null){
                LOGGER.error("没有可用的网关服务，无法接受短信");
                return inboundMessageList;
            }
            Service service = gatewayBean.getService();
            //接受新短信时回调
            service.setInboundMessageNotification(inboundNotification);
            //接受新电话时回调
            service.setCallNotification(callNotification);
            //gateway状态发生改变时回调
            service.setGatewayStatusNotification(statusNotification);
            //检测到不完整短信进行回调
            service.setOrphanedMessageNotification(orphanedMessageNotification);
            service.getKeyManager().registerKey(phone, new AESKey(new SecretKeySpec(Key.getBytes(), "AES")));
            service.readMessages(inboundMessageList, InboundMessage.MessageClasses.ALL);
            for (InboundMessage inboundMessage : inboundMessageList){
                int dstPort = inboundMessage.getDstPort();
                String text = inboundMessage.getText();
                String from = inboundMessage.getOriginator();
                long time = inboundMessage.getDate().getTime();
                Message.MessageEncodings encodings = inboundMessage.getEncoding();
                String gatewayId = inboundMessage.getGatewayId();
                Message.MessageTypes types = inboundMessage.getType();
                LOGGER.info("dstPort:[{}],text:[{}],from:[{}],time:[{}]",dstPort,text,from,time);
            }
        } catch (Exception e) {
            LOGGER.error("接受sms出现异常",e);
        }
        return inboundMessageList;
    }

    //新短信时回调
    class InboundNotification implements IInboundMessageNotification{
        @Override
        public void process(AGateway gateway, Message.MessageTypes msgType, InboundMessage msg) {
            if (msgType == Message.MessageTypes.INBOUND) {
                LOGGER.info(">>> New Inbound message detected from Gateway: " + gateway.getGatewayId());
            } else if (msgType == Message.MessageTypes.STATUSREPORT) {
                LOGGER.info(">>> New Inbound Status Report message detected from Gateway: " + gateway.getGatewayId());
            } else if (msgType == Message.MessageTypes.OUTBOUND){

            } else if (msgType == Message.MessageTypes.WAPSI){

            } else if (msgType == Message.MessageTypes.UNKNOWN){
            }
            System.out.println(msg);
        }
    }

    //接受新电话时回调
    class CallNotification implements ICallNotification {
        @Override
        public void process(AGateway gateway, String callerId) {
            LOGGER.info(">>> New call detected from Gateway: " + gateway.getGatewayId() + " : " + callerId);
        }
    }

    //gateway状态发生改变时回调
    class GatewayStatusNotification implements IGatewayStatusNotification {
        @Override
        public void process(AGateway gateway, AGateway.GatewayStatuses oldStatus, AGateway.GatewayStatuses newStatus) {
            LOGGER.info(">>> Gateway Status change for " + gateway.getGatewayId() + ", OLD: " + oldStatus + " -> NEW: " + newStatus);
        }
    }

    //检测到不完整短信进行回调
    class OrphanedMessageNotification implements IOrphanedMessageNotification {
        @Override
        public boolean process(AGateway gateway, InboundMessage msg) {
            LOGGER.info(">>> Orphaned message part detected from " + gateway.getGatewayId());
            LOGGER.info("text:{}",msg);
            return false;
        }
    }
}
