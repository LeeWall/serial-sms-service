package com.koinrocket.util;

import com.google.gson.Gson;
import com.koinrocket.bean.CallbackBean;
import com.koinrocket.common.Constant;
import com.koinrocket.entity.SmsQueueEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class CallbackUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(CallbackUtil.class);

    @Autowired
    @Qualifier(value = "configMap")
    private Map<String, String> configMap;

    private  Gson gson = new Gson();

    private String url;

    @PostConstruct
    void init(){
        if (configMap.get(Constant.CALLBACK_SMS_URL) == null){
            url = Constant.CALLBACK_SMS_DEFAULT_URL;
        }else {
            url = configMap.get(Constant.CALLBACK_SMS_URL);
        }
    }


    public Integer callbackSMS(SmsQueueEntity smsQueueEntity)  {
        long callbackTime = System.currentTimeMillis();
        Integer result = 0;
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        int code = 0;
        try {
            httpClient = HttpClients.createDefault();
            RequestConfig requestConfig = RequestConfig.custom().
                    setConnectTimeout(60000).
                    setSocketTimeout(120000).
                    setRedirectsEnabled(true).
                    build();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setConfig(requestConfig);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String message =  null;
            if (smsQueueEntity.getStatus() != 1){
                message = "phone:".concat(smsQueueEntity.getToPhone()).concat(" sms failed to send!");
            }
            params.add(new BasicNameValuePair("message",message));
            params.add(new BasicNameValuePair("messageId",smsQueueEntity.getMessageId()));
            params.add(new BasicNameValuePair("requestTime",String.valueOf(callbackTime)));
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            response = httpClient.execute(httpPost);
            if (response == null){
                code =  response.getStatusLine().getStatusCode();
                if (code == 200){
                    LOGGER.info("callback succeed,messageId:{}",smsQueueEntity.getMessageId());
                    String body = EntityUtils.toString(response.getEntity(), "UTF-8");
                    CallbackBean callbackBean = gson.fromJson(body, CallbackBean.class);
                    if (callbackBean.getCode() == 1){
                        result = 1;
                    }
                }else {
                    LOGGER.error("callback fail,messageId:{}",smsQueueEntity.getMessageId());
                    result = 99;
                }
            }
        }catch (Exception e){
                LOGGER.error("callback exception,messageId:{},exception:{}",smsQueueEntity.getMessageId(),e);
                result = 999;
        }finally {
            if (response != null){
                try {
                    response.close();
                } catch (IOException e) {
                    LOGGER.error("callback response closed exception,{}",e);
                }
            }
            if (httpClient != null){
                try {
                    httpClient.close();
                } catch (IOException e) {
                    LOGGER.error("callback httpClient closed exception,{}",e);
                }
            }
        }
        return result;
    }

}
