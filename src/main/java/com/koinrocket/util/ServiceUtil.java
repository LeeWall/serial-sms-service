package com.koinrocket.util;

import com.koinrocket.bean.GatewayBean;
import com.koinrocket.entity.ModemEntity;
import com.koinrocket.repository.ModemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
public class ServiceUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(ServiceUtil.class);

    @Autowired
    @Qualifier(value = "serviceMap")
    private Map<String, Service> serviceMap;

    @Autowired
    @Qualifier("gatewayMap")
    private Map<String, SerialModemGateway> modemGatewayMap;

    @Autowired
    private ModemRepository modemRepository;

    private Service service ;

    private String serialPort;

    @PostConstruct
    void initService(){
        if (!CollectionUtils.isEmpty(serviceMap)){
            serialPort = serviceMap.entrySet().iterator().next().getKey();
            service = serviceMap.entrySet().iterator().next().getValue();
        }
    }

    /**
     * 默认选择sim卡发送短信
     * @return
     */
    public GatewayBean defaultService(){
        GatewayBean gatewayBean = new GatewayBean();
        ModemEntity modemEntity = modemRepository.getBySerialPort(serialPort);
        String phoneNumber = modemEntity.getPhoneNumber();
        gatewayBean.setPhoneNumber(phoneNumber);
        gatewayBean.setService(service).setSerialPort(serialPort).setStatus(1);
        return gatewayBean;
    }

    /**
     * 指定手机号发送短信
     * @param phone
     * @return
     */
    public GatewayBean adjustService(String phone){
        GatewayBean gatewayBean = new GatewayBean();
        ModemEntity modemEntity = modemRepository.getByPhoneNumber(phone);
        if (modemEntity == null){
            LOGGER.warn("没有相关的串口配置");
            return gatewayBean.setStatus(0);
        }
        Service realService = null;
        String serialPort = modemEntity.getSerialPort();
        if (this.serialPort.equalsIgnoreCase(serialPort)){
            realService = this.service;
        }else{
            try {
                //停止服务，移除串口网关
                service.stopService();
                SerialModemGateway oldGateway = modemGatewayMap.get(this.serialPort);
                service.removeGateway(oldGateway);
                //加入新网关，开启服务
                SerialModemGateway newGateway = modemGatewayMap.get(serialPort);
                service.addGateway(newGateway);
                service.startService();
                realService = this.service;
                serviceMap.put(serialPort, realService);
            } catch (Exception e) {
               LOGGER.error("短信服务调整异常",e);
            }
        }
        return gatewayBean.setService(realService).setPhoneNumber(phone).setSerialPort(serialPort).setStatus(1);
    }

}
