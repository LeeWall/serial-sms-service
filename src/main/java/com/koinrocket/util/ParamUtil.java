package com.koinrocket.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParamUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParamUtil.class);

    private static PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    public static boolean checkPhone(String phone, String countryCode){
        Phonenumber.PhoneNumber  phoneNumber = null;
        boolean state = true;
        try {
            phoneNumber = phoneNumberUtil.parse(phone, countryCode);
            state =  phoneNumberUtil.isValidNumber(phoneNumber);
        } catch (NumberParseException e) {
            LOGGER.error("号码解析异常",e);
            return false;
        } finally {
            if(phoneNumber != null){
                phoneNumber.clear();
            }
        }
        return state;
    }
}
