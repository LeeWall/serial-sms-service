package com.koinrocket.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResultUtil<T> {

    private T data;

    public static <T> ResultUtil<T> result(T data){
        ResultUtil<T> result = new ResultUtil<>(data);
        return result;
    }

}
