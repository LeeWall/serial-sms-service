package com.koinrocket.util;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class SKUtils {

    public String sendSms(String url) throws IOException {
        ///CloseableHttpClient httpClient = null;
        String result = "";
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse httpResponse = null;
        int returnCode = 0 ;
        try {
            //httpClient = HttpClients.createDefault();
            //connectionRequestTimeout：指从连接池获取连接的timeout
            //connectionTimeout：指客户端和服务器建立连接的timeout
            //socketTimeout：指客户端从服务器读取数据的timeout，超出后会抛出SocketTimeOutException
            RequestConfig requestConfig = RequestConfig.custom().
                    setConnectTimeout(60000).
                    setSocketTimeout(120000).
                    setRedirectsEnabled(true).
                    build();
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept-Language","zh-CN");
            httpGet.setHeader("Accept","*/*");
            httpGet.setHeader("Connection","Close");
            //httpGet.setHeader("Connection","close");
            httpGet.setConfig(requestConfig);
            httpResponse = httpClient.execute(httpGet);
            if (httpResponse == null){
                return result;
            }else {
                returnCode = httpResponse.getStatusLine().getStatusCode();
                if (returnCode == 200 ){
                    result = EntityUtils.toString(httpResponse.getEntity());
                }
            }
        }finally {
            if (httpClient != null){
                httpClient.close();
            }
        }
        return result;
    }



}
