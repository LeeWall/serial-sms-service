package com.koinrocket.timer;

import com.koinrocket.common.Constant;
import com.koinrocket.entity.SmsQueueEntity;
import com.koinrocket.repository.MessageQueueRepository;
import com.koinrocket.service.ModemService;
import com.koinrocket.service.SmsConfigService;
import com.koinrocket.task.RetrySmsWork;
import com.koinrocket.task.SmsWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class SmsScheduledService {

    @Autowired
    private MessageQueueRepository messageQueueRepository;

    @Autowired
    private SmsConfigService smsConfigService;

    @Autowired
    @Qualifier(value = "configMap")
    private Map<String, String> configMap;

    @Autowired
    @Qualifier(value = "enableUsePortMap")
    private Map<String, AtomicInteger> enablePortMap;

    @Autowired
    @Qualifier(value = "notUsePortSet")
    private Set<String> notUseSet ;

    @Autowired
    @Qualifier(value = "portNotRegisterMap")
    private Map<String, AtomicInteger> portNotRegisterNumMap;

    @Autowired
    @Qualifier(value = "portCrossFailedMap")
    private Map<String, AtomicInteger> portCrossFailedNumMap;

    @Autowired
    private ModemService modemService;

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsScheduledService.class);

    private ExecutorService smsExecuteThreadPool = Executors.newFixedThreadPool(16);



    @Scheduled(cron = "0 0/3 * * * ?")
    public void send() {
        List<SmsQueueEntity> smsQueueEntityList = new ArrayList<>();
        int num = 200;
        //本次取出条数
        if (configMap.get(Constant.NOT_DEAL_SMS_NUM) != null){
            num = Integer.valueOf(configMap.get(Constant.NOT_DEAL_SMS_NUM));
        }
        //所有api级待发记录
        List<SmsQueueEntity> allAPINOTProcessList = messageQueueRepository.getALLAPINotProcess(num);
        Integer apiNotProcessSize = allAPINOTProcessList.size();
        //判断api级条数和取出条数
        if (apiNotProcessSize == num){
            smsQueueEntityList = allAPINOTProcessList;
        }else {
            Integer remain = num - apiNotProcessSize;
            //处理中的条数，这个时候处理中可能包含api级和普通级的短信记录，不分开那，处理中的记录条数小于单次拿出的条数
            List<SmsQueueEntity> allProcessingSmsList = messageQueueRepository.getALLProcessing();
            Integer allProcessingSize  = allProcessingSmsList.size();
            if (allProcessingSize >= remain){
                smsQueueEntityList.clear();
                smsQueueEntityList = allProcessingSmsList.subList(0,remain);
                smsQueueEntityList.addAll(allAPINOTProcessList);
            }else {
                remain = remain - allProcessingSize;
                List<SmsQueueEntity> notDealSmsList = messageQueueRepository.getAllNotDealMessage(remain);
                smsQueueEntityList.clear();
                smsQueueEntityList.addAll(allAPINOTProcessList);
                smsQueueEntityList.addAll(allProcessingSmsList);
                smsQueueEntityList.addAll(notDealSmsList);
            }
        }
        if (!CollectionUtils.isEmpty(smsQueueEntityList)){
            for (SmsQueueEntity smsQueueEntity : smsQueueEntityList){
                if (updatePutSmsStatus(smsQueueEntity) == 1){
                    smsExecuteThreadPool.submit(new SmsWork(smsQueueEntity, modemService));
                }
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Integer updatePutSmsStatus(SmsQueueEntity smsQueueEntity){
        Integer result = 1;
        try {
            smsQueueEntity.setStatus(-1).setPutStatus(1);
            messageQueueRepository.save(smsQueueEntity);
        }catch (Exception e){
            result = 0;
        }
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Integer updatePutSmsRetryStatus(SmsQueueEntity smsQueueEntity){
        Integer result = 1;

        try {
            smsQueueEntity.setRetryStatus(-1).setPutStatus(1);
            messageQueueRepository.save(smsQueueEntity);
        }catch (Exception e){
            result = 0;
        }
        return result;
    }


    @Scheduled(cron = "0 0/2 * * * ?")
    public void updateConfig(){
        smsConfigService.findAll();
    }


    @Scheduled(cron = "0 0/8 * * * ?")
    public void retryFailSms()  {
        List<SmsQueueEntity> smsQueueEntityList = new ArrayList<>();
        //取重试短信条数
        int retryNum = 100;
        if (configMap.get(Constant.RETRY_SMS_NUM) != null){
            retryNum = Integer.valueOf(configMap.get(Constant.RETRY_SMS_NUM));
        }
        //重试次数
        int retryTimes = 1;
        if (configMap.get(Constant.RETRY_SMS_TIMES) != null){
            retryTimes = Integer.valueOf(configMap.get(Constant.RETRY_SMS_TIMES));
        }
        List<SmsQueueEntity> allRetryProcessList = messageQueueRepository.getAllRetryProcessing(retryTimes);
        if (allRetryProcessList.size() >= retryNum && retryNum > 0){
            smsQueueEntityList = allRetryProcessList.subList(0, retryNum);
        }else {
            int part = retryNum - allRetryProcessList.size();
            List<SmsQueueEntity> notRetrySmsList =  messageQueueRepository.getRetryFailSms(part, retryTimes);
            smsQueueEntityList.addAll(allRetryProcessList);
            smsQueueEntityList.addAll(notRetrySmsList);
        }
        if (!CollectionUtils.isEmpty(smsQueueEntityList)){
            for (SmsQueueEntity smsQueueEntity: smsQueueEntityList){
                if (updatePutSmsRetryStatus(smsQueueEntity) == 1){
                    smsExecuteThreadPool.submit(new RetrySmsWork(smsQueueEntity, modemService));
                }
            }
        }
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void resetPort(){
        try {
            int portNo = 1 ;
            while (portNo <= 32){
                if (portNo == 18){
                    portNo ++ ;
                    continue;
                }
                enablePortMap.put(String.valueOf(portNo),new AtomicInteger(0));
                portNo ++;
            }
            notUseSet.clear();
            portNotRegisterNumMap.clear();
            portCrossFailedNumMap.clear();
        } catch (Exception e) {
            LOGGER.error("port status reset",e);
        }
    }


}
