package com.koinrocket.repository;

import com.koinrocket.entity.SmsConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmsConfigRepository extends JpaRepository<SmsConfigEntity, Integer> {

    @Query(value = "select * from t_sms_config where n_status=1", nativeQuery = true)
    List<SmsConfigEntity> findAllConfigs();
}
