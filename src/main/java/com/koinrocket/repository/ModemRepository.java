package com.koinrocket.repository;

import com.koinrocket.entity.ModemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.CopyOnWriteArrayList;

@Repository
public interface ModemRepository extends JpaRepository<ModemEntity, Integer> {

    @Query(value = "select * from t_serial_config  where n_status = 1 ", nativeQuery = true)
    CopyOnWriteArrayList<ModemEntity> finAllSerialPorts();

    @Query(value = "select * from t_serial_config where c_phone=:phoneNumber and n_status=1", nativeQuery = true)
    ModemEntity getByPhoneNumber(@Param("phoneNumber") String phoneNumber);

    @Query(value = "select * from t_serial_config where c_serial_port=:serialPort and n_status=1", nativeQuery = true)
    ModemEntity getBySerialPort(@Param("serialPort") String serialPort);

    @Modifying
    @Transactional
    @Query(value = "update t_serial_config set n_used_state = 0 where n_status = 1", nativeQuery = true)
    int initPortUsedState();

    @Modifying
    @Transactional
    @Query(value = "update t_serial_config set n_used_state = 1 where n_status = 1 and c_serial_port=:serialPort", nativeQuery = true)
    int updateUsedState(@Param("serialPort") String serialPort);
}
