package com.koinrocket.repository;

import com.koinrocket.entity.SmsQueueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageQueueRepository extends JpaRepository<SmsQueueEntity, Integer> {

    @Query(value = "select * from t_send_message_queue where n_status = 0 and n_try_count = 0 and n_put_status = 0 and n_priority = 1 order by t_receive_time asc limit :num", nativeQuery = true)
    List<SmsQueueEntity> getAllNotDealMessage(@Param("num") Integer num);

    @Query(value = "select * from t_send_message_queue where n_status = -1 and n_try_count = 0 and n_put_status = 1", nativeQuery = true)
    List<SmsQueueEntity> getALLProcessing();

    @Query(value = "select * from t_send_message_queue where n_status = 99 and n_retry_status = 0 and n_put_status = 0  and n_retry_count<:retryTimes " +
            "order by t_receive_time asc limit :retryNum", nativeQuery = true)
    List<SmsQueueEntity> getRetryFailSms(@Param("retryNum") Integer num, @Param("retryTimes") Integer retryTimes);

    @Query(value = "select * from t_send_message_queue where n_status = 99 and n_retry_status = -1 and n_put_status = 1  and n_retry_count<:retryTimes", nativeQuery = true)
    List<SmsQueueEntity> getAllRetryProcessing(@Param("retryTimes") Integer retryTimes);

    //取api级的待发短信
    @Query(value = "select * from t_send_message_queue where n_status = 0 and n_try_count = 0 and n_put_status = 0 and n_priority = 99 order by t_receive_time asc limit :fetchNum", nativeQuery = true)
    List<SmsQueueEntity> getALLAPINotProcess(@Param("fetchNum") Integer num);
}
