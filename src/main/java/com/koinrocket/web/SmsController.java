package com.koinrocket.web;

import com.koinrocket.bean.MessageInfo;
import com.koinrocket.common.Constant;
import com.koinrocket.dto.SmsQueueDTO;
import com.koinrocket.entity.SmsHistoryEntity;
import com.koinrocket.service.ModemService;
import com.koinrocket.util.ParamUtil;
import com.koinrocket.util.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 短信接口
 */
@RestController
@RequestMapping(value = "/sms")
public class SmsController {

    public static final Logger LOGGER = LoggerFactory.getLogger(SmsController.class);

    @Autowired
    private ModemService modemService;

    @Autowired
    @Qualifier(value = "configMap")
    private Map<String, String> configMap;

    @PostMapping("/send")
    public ResultUtil<List<MessageInfo>> send( @RequestParam(value = "from",required = false) String fromPhoneNumber,
                                               @RequestParam(value = "to") String toPhoneNumber,
                                               @RequestParam(value = "text") String text,
                                               @RequestParam(value = "device",required = false, defaultValue = "smsGateway") String device,
                                               @RequestParam(value = "priority", required = false, defaultValue = "1") Integer priority) throws UnsupportedEncodingException {
        LOGGER.info("receive new request ,to:{},text:{},priority:{}",toPhoneNumber,text,priority);
        List<MessageInfo> messageInfoList = new ArrayList<>();
        int length = Integer.MAX_VALUE;
        if(configMap.get(Constant.SMS_TEXT_LIMIT) != null){
            length = Integer.valueOf(configMap.get(Constant.SMS_TEXT_LIMIT));
        }
        if (text.toCharArray().length > length){
            messageInfoList.add(new MessageInfo().setCode(0).setMessageId(null).setTo(toPhoneNumber).setMsg("sms content char  is more than length limit").setTime(System.currentTimeMillis()));
            LOGGER.warn("to:{},content:{},sms content char length is more than length limit",toPhoneNumber,text);
            return ResultUtil.result(messageInfoList);
        }
        String[] toArray = toPhoneNumber.split(",");
        String countryCode = configMap.get(Constant.COUNTRY_CODE);
        String message = configMap.get(Constant.COUNTRY_PHONE_ILLEGAL_MESSAGE);
        for (String to:toArray){
            if (!ParamUtil.checkPhone(to, countryCode)){
                messageInfoList.add(new MessageInfo().setCode(0).setMessageId(null).setTo(to).setMsg(message).setTime(System.currentTimeMillis()));
                LOGGER.warn("to:{},phone is invalid",to);
                continue;
            }
            String messageId = modemService.sendSms(new SmsQueueDTO().setFromPhone(fromPhoneNumber).setToPhone(to).setText(text).setDevice(device).setPriority(priority));
            messageInfoList.add(new MessageInfo().setCode(1).setMessageId(messageId).setTo(to).setMsg("sms receive successfully, and wait to send!").setTime(System.currentTimeMillis()));
        }
        return ResultUtil.result(messageInfoList);
    }

    @GetMapping("/receive/{phoneNumber}")
    public ResultUtil<List<SmsHistoryEntity>> receive(@PathVariable String phoneNumber) {
        LOGGER.info("收取号码:[{}]的所有短信息",phoneNumber);
        List<SmsHistoryEntity> smsHistoryEntityList = modemService.receiveSms(phoneNumber);
        return ResultUtil.result(smsHistoryEntityList);
    }

}
