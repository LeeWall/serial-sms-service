package com.koinrocket.web;

import com.koinrocket.response.Result;
import com.koinrocket.service.SerialService;
import com.koinrocket.util.ResultUtil;
import gnu.io.CommPortIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 串口接口
 */
@RestController
@RequestMapping(value = "/serial")
public class SerialController {

    public static final Logger LOGGER = LoggerFactory.getLogger(SerialController.class);

    @Autowired
    private SerialService serialService;

    /**
     *  获取所有串口
     * @return
     */
    @GetMapping(value = "/findAllPort")
    public ResultUtil<List<CommPortIdentifier>> findAllPort(){
        List<CommPortIdentifier> commPortIdentifierList = serialService.findALlCommPort();
        return ResultUtil.result(commPortIdentifierList);
    }

    /**
     * 获取波特率
     * @return
     */
    @GetMapping(value = "/getBaudRate")
    public ResultUtil<String> getBaudRate(@RequestParam(name = "serialPort") String serialPort){
        int rate = 0;
        rate = serialService.getBaudRate(serialPort);
        if (rate == -1){
            return ResultUtil.result("获取失败");
        }
        return ResultUtil.result(String.valueOf(rate));
    }

}
