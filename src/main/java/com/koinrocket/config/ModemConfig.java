package com.koinrocket.config;

import com.koinrocket.entity.ModemEntity;
import com.koinrocket.repository.ModemRepository;
import com.koinrocket.repository.SmsConfigRepository;
import com.koinrocket.service.SerialService;
import com.koinrocket.service.SmsConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.AGateway;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadLocalRandom;


@Configuration
public class ModemConfig implements InitializingBean {

    public static final Logger LOGGER = LoggerFactory.getLogger(ModemConfig.class);

    @Autowired
    private ModemRepository modemRepository;

    @Autowired
    private SerialService serialService;

    @Autowired
    @Qualifier("gatewayMap")
    private Map<String, SerialModemGateway> modemGatewayMap;

    @Autowired
    @Qualifier("serviceMap")
    private Map<String, Service> serviceMap;

    @Autowired
    @Qualifier("serialVector")
    private Vector<String> vector;

    @Autowired
    private SmsConfigService smsConfigService;

    @Override
    public void afterPropertiesSet() {
        try {
            //初始化串口状态
           /* modemRepository.initPortUsedState();
            CopyOnWriteArrayList<ModemEntity> modemEntityList = modemRepository.finAllSerialPorts();
            if (CollectionUtils.isEmpty(modemEntityList)){
                LOGGER.warn("没有相关的串口配置,短信猫服务不能开启");
                return;
            }
            for (ModemEntity modemEntity : modemEntityList){
                String serialPort = modemEntity.getSerialPort();
                if (!serialService.isSerialPortEnabled(serialPort)){
                    modemEntityList.remove(modemEntity);
                }else {
                    vector.add(serialPort);
                }
            }
            if (CollectionUtils.isEmpty(modemEntityList)){
                LOGGER.warn("没有可用的串口,无法收发消息");
                return;
            }
            //初始化每个串口的网关
            for (ModemEntity modemEntity: modemEntityList){
                String serialPort = modemEntity.getSerialPort();
                SerialModemGateway gateway = new SerialModemGateway("modem".concat(serialPort) ,serialPort, modemEntity.getBaudRate(), modemEntity.getManufacturer(), modemEntity.getModel());
                gateway.setProtocol(AGateway.Protocols.PDU);
                gateway.setInbound(true); // 设置true，表示该网关可以接收短信
                gateway.setOutbound(true); // 设置true，表示该网关可以发送短信
                gateway.setSimPin("0000");  // sim卡锁，一般默认为0000或1234
                modemGatewayMap.put(serialPort, gateway);
            }
            //只能让一个网关加入到服务当中，随机选择一个网关,后续根据号码调整网关
            String serialPort = vector.get(ThreadLocalRandom.current().nextInt(vector.size()));
            modemRepository.updateUsedState(serialPort);
            SerialModemGateway serialModemGateway = modemGatewayMap.get(serialPort);
            //初始化短信服务
            Service service = Service.getInstance();
            //启用轮询
            Service.getInstance().getSettings().SERIAL_POLLING = true;
            //将网关加到服务中
            service.addGateway(serialModemGateway);
            //启动服务
            service.startService();
            serviceMap.put(serialPort, service);
            LOGGER.info("串口:[{}]被开启,短信服务启动",serialPort);*/
            //初始化全局配置
            smsConfigService.findAll();
        }catch (Exception e){
            LOGGER.error("串口初始化失败",e);
        }

    }
}
