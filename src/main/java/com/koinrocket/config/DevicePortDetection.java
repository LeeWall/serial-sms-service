package com.koinrocket.config;

import com.koinrocket.service.SmsConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Component(value = "devicePortDetection")
public class DevicePortDetection implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(DevicePortDetection.class);

    @Autowired
    private SmsConfigService smsConfigService;

    @Autowired
    @Qualifier(value = "enableUsePortMap")
    private Map<String, AtomicInteger> enablePortMap;

    @Override
    public void afterPropertiesSet() throws Exception {
        smsConfigService.findAll();
        int portNo = 0 ;
        while (portNo <= 32){
            portNo ++;
            //端口18无法使用
            if (portNo == 18){
                continue;
            }
            enablePortMap.put(String.valueOf(portNo),new AtomicInteger(0));
        }
    }
}
