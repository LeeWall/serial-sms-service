package com.koinrocket.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_serial_config")
public class ModemEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "n_id")
    private Integer id;

    @Column(name = "c_serial_port")
    private String serialPort ;

    @Column(name = "n_baud_rate")
    private int baudRate ;

    @Column(name = "c_manufacturer")
    private String manufacturer ;

    @Column(name = "c_model")
    private String model ;

    @Column(name = "c_phone")
    private String phoneNumber;

    @Column(name = "n_status")
    private Integer status;

    @Column(name = "n_used_state")
    private Integer usedState;

    @Column(name = "t_create_time")
    private Long createTime;

    @Column(name = "t_update_time")
    private Long updateTime;
}
