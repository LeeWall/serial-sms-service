package com.koinrocket.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "t_send_message_queue")
public class SmsQueueEntity implements Serializable {

    @Id
    @Column(name = "n_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "c_from")
    private String fromPhone;

    @Column(name = "c_to")
    private String toPhone;

    @Column(name = "c_text")
    private String text;

    @Column(name = "c_device_type")
    private String device;

    @Column(name = "c_serial")
    private String serial;

    @Column(name = "c_message_id")
    private String messageId;

    @Column(name = "n_status")
    private Integer status;

    @Column(name = "c_device_port")
    private String devicePort;

    @Column(name = "c_url")
    private String url;

    @Column(name = "c_result_reason")
    private String resultReason;

    @Column(name = "n_try_count")
    private Integer tryCount;

    @Column(name = "n_retry_count")
    private Integer retryCount;

    @Column(name = "n_retry_status")
    private Integer retryStatus;

    @Column(name = "n_callback_count")
    private Integer callbackCount;

    @Column(name = "n_callback_status")
    private Integer callbackStatus;

    @Column(name = "t_receive_time")
    private Long receiveTime;

    @Column(name = "t_send_time")
    private Long sendTime;

    @Column(name = "t_callback_time")
    private Long callbackTime;

    @Column(name = "t_retry_time")
    private Long retryTime;

    @Column(name = "t_api_execute_time")
    private Long apiExecuteTime;

    @Column(name = "n_put_status")
    private Integer putStatus;

    @Column(name = "n_priority")
    private Integer priority;

}
