package com.koinrocket.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "t_sms_config")
public class SmsConfigEntity implements Serializable {

    @Id
    @Column(name = "n_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "c_key")
    private String key;

    @Column(name = "c_value")
    private String value;

    @Column(name = "n_status")
    private Integer status;

    @Column(name = "t_create_time")
    private Long createTime;

    @Column(name = "t_update_time")
    private Long updateTime;
}
