package com.koinrocket.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "t_sms_history")
public class SmsHistoryEntity implements Serializable {

    @Id
    @Column(name = "n_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "c_from")
    private String fromPhoneNumber;

    @Column(name = "c_to")
    private String receivePhoneNumber;

    @Column(name = "c_text")
    private String text;

    @Column(name = "t_receive_time")
    private long receiveTime;
}
