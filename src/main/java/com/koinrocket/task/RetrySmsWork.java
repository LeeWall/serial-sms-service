package com.koinrocket.task;

import com.koinrocket.entity.SmsQueueEntity;
import com.koinrocket.service.ModemService;

public class RetrySmsWork implements Runnable {

    private SmsQueueEntity smsQueueEntity;

    private ModemService modemService;

    public RetrySmsWork(SmsQueueEntity smsQueueEntity, ModemService modemService) {
        this.smsQueueEntity = smsQueueEntity;
        this.modemService = modemService;
    }

    @Override
    public void run() {
        modemService.retry(smsQueueEntity);
    }
}
