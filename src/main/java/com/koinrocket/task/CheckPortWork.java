package com.koinrocket.task;

import com.koinrocket.bean.SKResponseBean;
import com.koinrocket.util.GsonUtil;
import com.koinrocket.util.SKUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CheckPortWork implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckPortWork.class);

    private SKUtils skUtils;

    private String url;

    private Integer key;

    private List<String> enableList;


    public CheckPortWork(SKUtils skUtils, String url, Integer key, List<String> enableList) {
        this.skUtils = skUtils;
        this.url = url;
        this.key = key;
        this.enableList = enableList;
    }

    @Override
    public void run() {
        try {
            LOGGER.info("checkPort,url:[{}]",url);
            String result = skUtils.sendSms(url);
            SKResponseBean skResponseBean = GsonUtil.GsonToBean(result, SKResponseBean.class);
            if (skResponseBean != null){
                String reason = skResponseBean.getReason();
                if (!"Not Registered".equalsIgnoreCase(reason) || !"SIM not registered".equalsIgnoreCase(reason)){
                    enableList.add(String.valueOf(key));
                }
            }
        } catch (Exception e) {
           LOGGER.error("check port exception",e);
        }
    }
}
