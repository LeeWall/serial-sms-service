package com.koinrocket.task;

import com.koinrocket.entity.SmsQueueEntity;
import com.koinrocket.service.ModemService;

public class SmsWork implements Runnable{

    private SmsQueueEntity smsQueueEntity;

    private ModemService modemService;

    public SmsWork(SmsQueueEntity smsQueueEntity, ModemService modemService) {
        this.smsQueueEntity = smsQueueEntity;
        this.modemService = modemService;
    }

    @Override
    public void run() {
        modemService.execute(smsQueueEntity);
    }
}
