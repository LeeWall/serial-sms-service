package com.koinrocket.filter;

import com.google.gson.Gson;
import com.koinrocket.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Map;

@Component
@WebFilter(urlPatterns = "/sms/*",filterName = "smsFilter")
public class smsFilter implements Filter {

    @Autowired
    @Qualifier(value = "configMap")
    private Map<String, String> configMap;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        // 这里填写你允许进行跨域的主机ip
        response.setHeader("Access-Control-Allow-Origin","*");
        // 允许的访问方法
        response.setHeader("Access-Control-Allow-Headers","POST");
        // Access-Control-Max-Age 用于 CORS 相关配置的缓存
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers","token,Origin, X-Requested-With, Content-Type, Accept");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        String token = request.getHeader("token");
        Result result = new Result();
        boolean isFilter = false;
        if (StringUtils.isEmpty(token)){
            result.setCode(9000).setMessage("客户端请求中没有token信息");
        }else {
            if (!token.equals(configMap.get("token"))){
                result.setCode(9001).setMessage("客户端请求的token信息不正确");
            }else {
                result.setCode(9999).setMessage("客户端请求的token验证成功");
                isFilter = true;
            }
        }
        if (result.getCode() != 9999){
            OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), "UTF-8");
            PrintWriter writer = new PrintWriter(osw, true);
            Gson gson = new Gson();
            String jsonStr = gson.toJson(result);
            writer.write(jsonStr);
            writer.flush();
            writer.close();
            osw.close();
        }
        if (isFilter){
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() { }
}
