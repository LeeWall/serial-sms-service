package com.koinrocket.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Setter
@Getter
@Accessors(chain = true)
public class SmsQueueDTO implements Serializable {

    private String fromPhone;

    private String toPhone;

    private String text;

    private String device;

    private Integer priority;

}
