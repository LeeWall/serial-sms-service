package com.koinrocket.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Setter
@Getter
@Accessors(chain = true)
public class SKSmsDTO implements Serializable {

    private String username;

    private String password;

    private String port;

    private String recipients;

    private String text;

    private String charset = "Utf-8";

    private Integer timeout = 120;
}
