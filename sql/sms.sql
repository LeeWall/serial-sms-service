DROP TABLE IF EXISTS `t_send_message_queue`;
CREATE TABLE `t_send_message_queue`
(
  `n_id`           int(11) NOT NULL AUTO_INCREMENT,
  `c_from`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发信人',
  `c_to`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收信人',
  `c_text`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短信内容',
  `c_serial`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '串口名称',
  `c_message_id`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短信标识ID，32位长度',
  `n_status`       int(4) NULL DEFAULT NULL COMMENT '短信状态',
  `t_receive_time` bigint(20) NULL DEFAULT NULL COMMENT '短信接收时间',
  `t_send_time`    bigint(20) NULL DEFAULT NULL COMMENT '短信发送时间',
  PRIMARY KEY (`n_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `t_serial_config`;
CREATE TABLE `t_serial_config`
(
  `n_id`           int(11) NOT NULL AUTO_INCREMENT,
  `c_serial_port`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '串口名称',
  `n_baud_rate`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '波特率',
  `c_model`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '型号',
  `c_manufacturer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂商',
  `c_phone`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'sim卡对应的手机号',
  `n_status`       int(255) NULL DEFAULT NULL COMMENT '可用状态',
  `t_create_time`  bigint(20) NULL DEFAULT NULL COMMENT '创建时间',
  `t_update_time`  bigint(20) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`n_id`) USING BTREE,
  UNIQUE INDEX `comm_port`(`c_serial_port`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `t_sms_config`;
CREATE TABLE `t_sms_config`
(
  `n_id`          int(11) NOT NULL AUTO_INCREMENT,
  `c_key`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名称',
  `c_value`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段值',
  `n_status`      int(255) NULL DEFAULT NULL COMMENT '字段有效性',
  `t_create_time` bigint(20) NULL DEFAULT NULL COMMENT '创建时间',
  `t_update_time` bigint(20) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`n_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `t_sms_history`;
CREATE TABLE `t_sms_history`
(
  `n_id`           int(11) NOT NULL AUTO_INCREMENT,
  `c_from`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送号码',
  `c_to`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受号码',
  `c_text`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短信内容',
  `t_receive_time` bigint(20) NULL DEFAULT NULL COMMENT '短信接受时间',
  PRIMARY KEY (`n_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE EVENT `back_up_sms_event`
on SCHEDULE EVERY 1 DAY STARTS '2019-01-15 17:02:00'
on COMPLETION not PRESERVE	ENABLE DO CALL `update_message_pro`();

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_message_pro`()
BEGIN
set @back_table_name = CONCAT('t_send_message_queue_',DATE_FORMAT(now(),"%Y_%m_%d"));
set @updateSql=CONCAT('ALTER TABLE t_send_message_queue RENAME TO ',@back_table_name);
PREPARE STMT FROM @updateSql;
EXECUTE STMT;
set @createSql = CONCAT('CREATE TABLE t_send_message_queue  like ',@back_table_name);
PREPARE STMT FROM @createSql;
EXECUTE STMT;
set @deleteSql = CONCAT('TRUNCATE TABLE t_send_message_queue');
PREPARE STMT FROM @deleteSql;
EXECUTE STMT;
end